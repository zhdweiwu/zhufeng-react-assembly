module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links", // a标签 https://github.com/storybookjs/storybook/tree/master/addons/links
    "@storybook/addon-essentials", 
    "@storybook/preset-create-react-app",
    "@storybook/addon-actions", // 控制台 https://github.com/storybookjs/storybook/tree/master/addons/actions
    { name: "@storybook/addon-docs", options: { configureJSX: true } }
  ],
  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [
        {
          loader: require.resolve("react-docgen-typescript-loader"),
        },
      ],
    });
    config.resolve.extensions.push(".ts", ".tsx");
    return config;
  },

}