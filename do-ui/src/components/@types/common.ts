import React from 'react';

export interface StandarProps {
    /**
     * 前缀
     */
    classPrefix?: string
    
    /**
     * 类名
     */
    className?: string

    /**
     * 子元素
     */
    children?: React.ReactNode

    /**
     * 行内样式
     */
    style?: React.CSSProperties

    /**
     * 组件名字
     */
    // displayName: string
}

export declare namespace TypeAttributes {
    /**
     *     font   height
     * @xs 12     24
     * @xs 12     28
     * @xs 14     32
     * @xs 16     46
     */
    type Size = 'xs' | 'sm' | 'md' | 'lg'

    /**
     * 不用 success error 
     */
    type Color = 'primary' | 'normal' | 'error' | 'success' | 'warning' 
    // | 'red' | 'orange' | 'yellow' | 'green' | 'blue'

    type Type = 'normal' | 'ghost' | 'dashed' | 'text' | 'link'

    type HtmlType = 'submit' | 'reset' | 'button'

    type Shape = 'normal' /* 4px */ | 'circle' | 'round'
}