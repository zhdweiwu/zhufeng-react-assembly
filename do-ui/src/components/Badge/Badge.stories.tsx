import React from "react";
import { Badge } from "./index";
import {
	withKnobs,
	text,
	boolean,
	color,
	select,
} from "@storybook/addon-knobs";

export default {
	title: "Badge",
	component: Badge,
	decorators: [withKnobs],
};

export const knobsBtn = () => <div>Badge</div>;