import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Button, {ButtonProps} from './index.tsx';
const SIZES = ['xs', 'sm', 'md', 'lg'];
const SHAPE = ['normal' /* 4px */ , 'circle' , 'round'];
const TYPES = ['normal' , 'ghost' , 'dashed' , 'text' , 'link']
const COLORS = ['normal' , 'primary', 'error' , 'success' , 'warning'];

export default {
  title: 'Example/Button',
  component: Button,
//   argTypes: {
//     backgroundColor: { control: 'color' },
//   },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

// export const Type = Template.bind({});
// Type.args = {
//   children: 'Button',
// };
// Type.args = {
//   children: 'Button',
// };
// Type.args = {
//   children: 'Button',
// };


export const defaults = () => {
  return <>
      <h1>SIZE</h1>
        <div>
          {
        SIZES.map(size => {
          return <Template
            size={size}
          >{size}</Template>
        })
        }
        </div>



{/* -------------------------------------------------------------------------------------------------------------- */}
    <h1>COLOR</h1>
    <div>
      {
        COLORS.map(color => {
          return <Template color={color}>{color}</Template>
        })
      }
    </div>
    <br/>

    <div>
      {
        COLORS.map(color => {
          return <Template size={'sm'} color={color}>{color}</Template>
        })
      }
    </div>
    <br/>

    <div>
      {
        COLORS.map(color => {
          return <Template size={'md'} color={color}>{color}</Template>
        })
      }
    </div>
    <br/>
    <div>
      {
        COLORS.map(color => {
          return <Template size={'lg'} color={color}>{color}</Template>
        })
      }
    </div>
{/* -------------------------------------------------------------------------------------------------------------- */}

      <h1>TYPE</h1>
    <div>
      {
        TYPES.map(type => {
          return <>
          <br/>
          <div>
            {
              COLORS.map(color => {
                return <Template color={color} type={type}>{type}</Template>
              })
            }
          </div>
          </>
        })

      }
    </div>


{/* -------------------------------------------------------------------------------------------------------------- */}

      <h1>shape</h1>
      <div>
      {
    SHAPE.map(shape => {
    return <>
    <br/>
    {
            TYPES.slice(0, 3).map(type => {
              return <>
              <br/>
              <div>
                {
                  COLORS.map(color => {
                    return <Template shape={shape} color={color} type={type}>1</Template>
                  })
                }
              </div>
              </>
            })
    
          }
    </>
    })
  }
  <br/>
          
      </div>

      </>
}
