import React from 'react';
import {render,  fireEvent} from '@testing-library/react';
import Button, {ButtonProps} from './index';

const defaultProps = {
    onClick: jest.fn(),
    className: 'testProps',
    'data-testid': 'increase-button'
}

const testProps: ButtonProps = {
    type: 'normal',
    size: 'sm',
    className: 'testProps'
}

const disabledProps: ButtonProps = {
    disabled: true,
    onClick: jest.fn()
};


describe('test Button componet', () => {
    it('should render the correct default button', () => {
        const wrapper = render(<Button {...defaultProps}>hello</Button>);
        const ele = wrapper.getByTestId('increase-button');
        // console.log(ele, 'wrapper')
        expect(ele).toBeInTheDocument();

        // 正确渲染文本
        const text = wrapper.getByText('hello');
        expect(text).toBeTruthy();

        // button
        expect(ele.tagName).toEqual('BUTTON');
        expect(ele).not.toHaveAttribute("isdisabled");
        expect(ele).not.toHaveAttribute("isLinked");


        // // 正常添加class
        // expect(
		// 	ele.getAttribute("class")
		// 		?.split(" ")
		// 		.includes("testprops")
        // ).toEqual(true);
        // disabledProps.onClick();
        // fireEvent.click(ele);
		// expect(defaultProps.onClick).toHaveBeenCalled();
		// //span正常显示
		// expect(ele.getElementsByTagName("span")).toBeTruthy();
		// //正常默认属性
		// expect(ele).toHaveStyle(`background:${color.tertiary}`);
		// expect(ele).toHaveStyle(`color: ${color.darkest}`);
		// //正常大小
		// expect(ele).toHaveStyle(`padding: ${btnPadding.medium}`);
		// expect(ele).toHaveStyle(`font-size:${typography.size.s2}px`);


    })
})