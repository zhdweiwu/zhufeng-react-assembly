import React from 'react';
import {TypeAttributes, StandarProps} from '../@types';
import {isUndefined, isFunction, c} from '../../utils'
import omit from 'omit.js';

export interface ButtonProps extends StandarProps {
    /**
     * size
     */
    size?: TypeAttributes.Size
    /**
     * 类型
     *  - default normal
     */
    type?: TypeAttributes.Type
    
    /**
     * 是否禁用
     * - default false
     */
    disabled?: boolean

    /**
     * 处于loading 状态
     * - default false 
     */
    loading?: boolean
    
    /**
     * 是否与赋元素同宽
     * - default false
     */
    block?: boolean

    /**
     * 点击事件
     * onClick
     *  - return promise | function
     *      promise 开启防连击 loading  直到 promise 返回才会结束 有 loading 参数不会 出现
     *      function 需要自己控制loading
     */
    onClick?: React.MouseEventHandler<HTMLElement>

    /**
     * 原生type submit reset button
     */
    htmlType?: TypeAttributes.HtmlType

    /**
     * 
     */
    color?: TypeAttributes.Color

    /**
     * 形状
     */
    shape?: TypeAttributes.Shape


    /**
     * href
     * - 有href 自动是<a>
     */
    href?: string
    
    /**
     * 目标元素
     * - a 时才有
     */
    target?: string

    /**
     * icon
     */
    icon?: React.ReactNode

    /**
     * loading-icon
     */
    loadingIcon?:  React.ReactNode

    /**
     * loading 延时
     * - boolean  false = 0 true = 300
     * - number
     */
    loadingDelay?: boolean | number

}
class Button extends React.Component<ButtonProps> {

    static displayName = 'Button';

    static defaultProps = {
        className: '',
        disabled: false,
        size: 'xs',
        type: 'normal',
        shape: 'normal',
        loadingDelay: 300,
        color: 'normal'
        
    } as ButtonProps

    get className () {
        const {size, type, color, className, shape, disabled} = this.props;

        return c('btn', className, {
            [`btn-size-${size}`]: size,
            [`btn-type-${type}`]: type,
            [`btn-color-${color}`]: color,
            [`btn-shape-${shape}`]: shape
            // [`btn-disabled`]: disabled,
        });
    }

    get buttonProps() {
        return {
            ...omit(this.props, [
                'color',
                'htmlType',
                'href',
                'type',
                'loading',
                'loadingDelay',
                'shape',
                'className',
                'classPrefix',
                'onClick',
                'shape',
                'icon',
                'loadingIcon'
            ]),
            className: this.className,
            onClick: this.handleClick
        }
    }
    get Component () {
        return this.props.type === 'link' ? 'a' : 'button';
    }

    handleClick:ButtonProps['onClick'] = e => {
        const {disabled, loading, onClick}:ButtonProps = this.props;
        if (disabled || loading || !isFunction(onClick)) {
            return ;
        }
        const result = onClick(e);
    }

    render() {
        const {
            children
        } = this.props;
        const {Component} = this;

        const {href} = this.props;
        return isUndefined(href)
        ? <Component {...this.buttonProps}>{children}</Component>
        : <a>{children}</a>
    }
}

export default Button;
