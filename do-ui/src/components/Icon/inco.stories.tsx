import React from "react";
import Icon, {IconProps} from './index';
import { withKnobs, color, select } from "@storybook/addon-knobs";
import {icons} from './icons';
import './demo.css';

export default {
    title: 'Icon',
    component: Icon,
    decorators: [withKnobs],
}


export const knobsIcon = () => (
    <Icon
        svg={select<IconProps["svg"]>(
            "icons",
            Object.keys(icons) as IconProps["svg"][],
            "bookmark"
        )}
        color={color("color", "black")}
    ></Icon>
);


export const labels = () => (
	<>
		There are {Object.keys(icons).length} icons
		<ul>
			{Object.keys(icons).map((key) => (
				<li key={key}>
					<Icon svg={key as keyof typeof icons} color={'#f40'}  />
					<div>{key}</div>
				</li>
			))}
		</ul>
	</>
);