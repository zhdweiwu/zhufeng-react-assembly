import React from 'react';
import { icons } from './icons';
import {TypeAttributes, StandarProps} from '../@types';
import {isUndefined, isFunction, c} from '../../utils'
import omit from 'omit.js';


export interface IconProps {
    svg: keyof typeof icons
    /**
     * 
     */
    block?: boolean
    /**
     * 
     */
    color?: string
}

export default function Icon(props: IconProps) {
    const { block, svg, color } = props;
    const {} = props;
        return <span>
        <svg
        viewBox="0 0 1024 1024"
        width="20px"
        height="20px"
        block={block}
        {...props}
        >
        <path d={icons[svg]} color={color} />
        </svg></span>
}

Icon.defaultProps = {
	block: false,
	color: "black",
};