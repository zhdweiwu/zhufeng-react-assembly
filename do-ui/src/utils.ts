import classname from 'classnames';

const toString = Object.prototype.toString;
export function isUndefined(val: any): val is undefined {
    return typeof val === 'undefined';
}

export function isFunction(val: any): val is Function {
    return  typeof val === 'function';
}

export function prefix(prefix: string = ''): any {
    return function(...args: any) {
        return classname(...args).split(' ').map((classname:any) => prefix + classname).join(' ');
    }
}
export const c = prefix('do-');